
import React from 'react';
import { render} from 'react-dom';
import { hot } from 'react-hot-loader'
import  Dashboard from './scenes/Dashboard'
const App = () => (
    <Dashboard></Dashboard>
)

if (process.env.NODE_ENV !== 'production')  {
    hot(module)(App)
}
render(<App />, document.getElementById("root"))