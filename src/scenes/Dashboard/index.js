import React from 'react'
import './styles.css'
import vylan from '../../../public/images/vylan.png'

export default class Dashboard extends React.Component {
  render() {
    return (
      <div>
        <h1>Đoàn Đại Hồ test cache-control env {process.env.NODE_ENV}</h1>
        <img src={vylan}></img>
      </div>
    )
  }
}