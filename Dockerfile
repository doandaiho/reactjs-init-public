# Stage 1 - the build process
FROM mhart/alpine-node as builder
MAINTAINER hodd

WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn
COPY . /usr/src/app

# <-- Build for testing !-->
RUN yarn build:testing
# <-- Build for uat !-->
# RUN yarn build:uat
# <-- Build for production !-->
# RUN yarn build

# Stage 2 - the production environment
FROM nginx:stable-alpine
COPY --from=builder /usr/src/app/dist/ /usr/share/nginx/html/
COPY nginx.conf /etc/nginx/
ADD ./docker-run.sh /usr/share/nginx/

EXPOSE 8080

CMD ["sh", "/usr/share/nginx/docker-run.sh"]
