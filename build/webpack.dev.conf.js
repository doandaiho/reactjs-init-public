'use strict'
const path = require('path')
const webpack = require('webpack')
const config = require('../config')
const merge = require('webpack-merge')
const baseWebpackConfig = require('./webpack.base.conf')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')

const dev = merge(baseWebpackConfig, {
  devServer: {
    port: config.dev.port,
    hot: true,
    hotOnly: true
  },
  // cheap-module-eval-source-map is faster for development
  devtool: config.dev.devtool,
  plugins: [
    new webpack.DefinePlugin({
      'process.env': require('../config/dev.env'),
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(), // HMR shows correct file names in console on update.
    new webpack.NoEmitOnErrorsPlugin(),
    new FriendlyErrorsPlugin({
      compilationSuccessInfo: {
        messages: [`Your application is running on ${process.env.NODE_ENV}`],
      },
      onErrors: config.dev.notifyOnErrors
      ? function () {
        const notifier = require('node-notifier')

        return (severity, errors) => {
          if (severity !== 'error') {
            return
          }
          const error = errors[0]
      
          const filename = error.file && error.file.split('!').pop()
          notifier.notify({
            title: pkg.name,
            message: severity + ': ' + error.name,
            subtitle: filename || ''
          })
        }
      } : undefined
    })
  ]
})
module.exports = dev